import { FaCheckCircle, FaTimesCircle } from 'react-icons/fa'

const Character = ({ character }) => {
    const isOwned = () => {
        return character.owned ? <FaCheckCircle color='green' /> : <FaTimesCircle color='red' />
    }

    return (
        <div className='task'>
            <h3>{character.name} ({character.element}) {isOwned()}</h3>
            <p>{character.rarity}</p>
            <p>Level: {character.level}</p>
            <p>Ascension: {character.ascend}</p>
            <p>Constellation: {character.constellation}</p>
        </div>
    )
}

export default Character
