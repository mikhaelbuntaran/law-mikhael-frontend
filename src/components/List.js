import Character from './Character'

const List = ({ list }) => {
    return (
        <>
            {list.map((character) => (
                <Character
                    key={character.id}
                    character={character}
                />
            ))}
        </>
    )
}

export default List
