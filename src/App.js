import { useState, useEffect } from 'react'
import { FaCheckCircle, FaTimesCircle } from 'react-icons/fa'
import Header from './components/Header'
import List from './components/List'

function App() {
  const [list, setList] = useState([])

  useEffect(() => {
    const fetchList = async () => {
      await fetch('http://localhost:8000/genshin-chars')
        .then(res => res.json())
        .then((data) => {
          console.log(data)
          setList(data)
        })
        .catch(console.log('Error in fetching the Genshin Char List'))
    }
    fetchList()
  }, [])

  return (
    <div className="container">
      <Header title='My Genshin Character List' />
      <p><FaCheckCircle color='green' /> owned</p>
      <p><FaTimesCircle color='red' /> not owned</p>
      <List list={list} />
    </div>
  );
}

export default App;
